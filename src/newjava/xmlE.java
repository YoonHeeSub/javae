package newjava;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


public class xmlE {
	

	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
		/// parse 하는 방법 다양함 
		//오라클 홈페이지에서 java8에맞는거 검색해서 사용하기
		
		
		File file = new File("abc.xml");
		// 파싱할 xml 가져오기   or  InputSource is = new InputSource(new StringReader(xml)); 
		
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		//  XML 문서에서 DOM 개체 트리를 생성하는 파서를 얻을 수 있도록 하는 도큐먼트빌더 팩토리 API를 정의
		// 새로운인스턴스만들어준다.
		
        DocumentBuilder db = dbf.newDocumentBuilder();
        // XML 문서에서 DOM 문서 인스턴스를 가져오는 API를 정의합니다
        // 이 클래스의 인스턴스를 얻으면 다양한 입력 소스에서 XML을 구문 분석할 수 있습니다.
        
        
        //Simple API for XML  = > sax api
        
        
        Document document = db.parse(file);
        //Document인터페이스는 전체 HTML이나 XML 문서를 나타냅니다
        // 위에가져온xml파일을  도빌.파서를 통해 파싱?
        
        
        document.getDocumentElement().normalize();
        //겟도엘 >  문서의 문서 요소인 자식 노드에 직접 접근할 수 있는 편의 속성입니다.
        //돔트리를  xml 구조대로 만들어주는것
        
        
        
        //   xml이있는  파일 주소 알려주기  >> xml구조로 바꾸기위해 다양한 객체들 인스턴스화
        // >> 최종적으로    .getDocumentElement().normalize() 를 사용하여  돔트리를 xml 문서구조로바꿔준다
        System.out.println("Root Element :" + document.getDocumentElement().getNodeName());
        // 최상위 노드 명 가져오기
 
        NodeList nList = document.getElementsByTagName("employee");
        //XML 데이터 처리용의 Java API 의 컴퍼넌트 API 인 DOM (Document Object Model)의 인터페이스를 제공합니다.
        
        // 주어진  태그명과 함께, 모든 자손 Elements 의 NodeList 를 문서순서에 돌려줍니다.
        
        
        
        System.out.println("----------------------------");
        
        
        HashMap<String, String> hm = new HashMap<String, String>();
        //맵 사용하여서 데이터 넣어보기 ..
        
        
        ArrayList<String> arrListId = new ArrayList<String>();
        ArrayList<String> arrListFirstName = new ArrayList<String>();
        ArrayList<String> arrListLastName = new ArrayList<String>();
        ArrayList<String> arrListSalary = new ArrayList<String>();
        ///arrlist 사용해보기 >> 가변 배열 //

        String id ="";
        String firstName ="";
        String lastName ="";
        String salary ="";
        
       //값넣어줄 변수 선언 
        
        for (int temp = 0; temp < nList.getLength(); temp++) {
        	//getlength로 xml 길이 보다작을때까지 반복해주고..
        	
            Node nNode = nList.item(temp);
           
            
            System.out.println("\nCurrent Element :" + nNode.getNodeName());
            
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
            	// nList.item(temp)의 유형과  == nlist의 아이템 의 상수값과비교?
                Element eElement = (Element) nNode;
                //Element인터페이스는 HTML이나 XML 문서 내의 요소를 나타냅니다\
                // 이행위를 안할시 ㄲ!%#@^%&$%이런식으로 표현됨 
               
                System.out.println("Employee id : " + eElement.getAttribute("id"));
                //                  이름으로 속성 값을 검색합니다.
                id = eElement.getAttribute("id");
                
                System.out.println("First Name : " + eElement.getElementsByTagName("firstname").item(0).getTextContent());
                
                firstName = eElement.getElementsByTagName("firstname").item(0).getTextContent();
                
                System.out.println("Last Name : " + eElement.getElementsByTagName("lastname").item(0).getTextContent());
                
                lastName = eElement.getElementsByTagName("lastname").item(0).getTextContent();
                
                System.out.println("Salary : " + eElement.getElementsByTagName("salary").item(0).getTextContent());
                
                salary = eElement.getElementsByTagName("salary").item(0).getTextContent();
                
               // 여까지 xml 데이터를 꺼내서 변수 에 담아주기작업
                
               // put해서  해쉬에 키 벨류 넣어주기
                hm.put(temp+"id",id);
                hm.put(temp+"firstName",firstName);
                hm.put(temp+"lastName",lastName);
                hm.put(temp+"salary",salary);
                
                //arry에 추가해주기
                arrListId.add(temp, id);
                arrListFirstName.add(temp,firstName);
                arrListLastName.add(temp, lastName);
                arrListSalary.add(temp,salary);
            }
               
        }
        System.out.println("--------");
        System.out.println("hashmap에 넣어준값들 꺼내보기"+"\n"+"----------");
        for (int i = 0; i < 3; i++) {
        
        	 System.out.println(hm.get(i+"id"));
        	 System.out.println(hm.get(i+"firstName"));
        	 System.out.println(hm.get(i+"lastName"));
        	 System.out.println(hm.get(i+"salary"));
        	 System.out.println("------"); 
		}
    
        
        
        System.out.println("arryList에 넣어준값들 꺼내보기\n"+"-----");
       for (int i = 0; i < 3; i++) {
    	   System.out.println(arrListId.get(i));
           System.out.println(arrListFirstName.get(i));
           System.out.println(arrListLastName.get(i));
           System.out.println(arrListSalary.get(i));
           System.out.println("------");
	}       
		     
		      
		      
		      
		
		
	}

}
