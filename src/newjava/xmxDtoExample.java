package newjava;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.sun.javafx.scene.paint.GradientUtils.Parser;
import com.sun.org.apache.xerces.internal.jaxp.DocumentBuilderFactoryImpl;
import javafx.util.BuilderFactory;
import xml.dto.dto;

public class xmxDtoExample {

	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
		
		File file = new File("abc.xml");
		
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		
		DocumentBuilder db = dbf.newDocumentBuilder();
		
		Document docm =db.parse(file);
		
		docm.getDocumentElement().normalize();
		
		NodeList nList = docm.getElementsByTagName("employee");
		
		dto dto = new dto();
		ArrayList<String> arr = new ArrayList<String>();
		
		for (int i = 0; i < nList.getLength(); i++) {
			Node node = nList.item(i);
			System.out.println(node.getNodeType());
			// xml이 존재하면 1을 반환하고 다지우니 아무것도 반환하지않앗음..
			System.out.println(Node.ELEMENT_NODE);
			
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				Element el = (Element) node;
				System.out.println("Employee id : " + el.getAttribute("id"));
                
                System.out.println("First Name : " + el.getElementsByTagName("firstname").item(0).getTextContent());     
           
                System.out.println("Last Name : " + el.getElementsByTagName("lastname").item(0).getTextContent());
                
                System.out.println("Salary : " + el.getElementsByTagName("salary").item(0).getTextContent());
                
              dto.setEmployeeId(el.getAttribute("id"));
              dto.setFirstName(el.getElementsByTagName("firstname").item(0).getTextContent());
              dto.setLastName(el.getElementsByTagName("lastname").item(0).getTextContent()); 
              dto.setSalary(el.getElementsByTagName("salary").item(0).getTextContent());
              //dto에담은값
   
			}

			arr.add(dto.getEmployeeId());
			arr.add(dto.getFirstName());
			arr.add(dto.getLastName());
			arr.add(dto.getSalary());
			
		}
		
		for (int i = 0; i < arr.size(); i++) {
			System.out.println(arr.get(i));
		}
		
	}
	
	
	
	
	

}
