package xml.dto;

public class dto {
	private String EmployeeId;
	private String FirstName;
	private String LastName;
	private String Salary;
	
	
	public String getEmployeeId() {
		return EmployeeId;
	}
	public void setEmployeeId(String string) {
		EmployeeId = string;
	}
	public String getFirstName() {
		return FirstName;
	}
	public void setFirstName(String firstName) {
		FirstName = firstName;
	}
	public String getLastName() {
		return LastName;
	}
	public void setLastName(String lastName) {
		LastName = lastName;
	}
	public String getSalary() {
		return Salary;
	}
	public void setSalary(String string) {
		Salary = string;
	}
	
}
